package main

import (
	"fmt"
	"lru/internal/lruCache"
)

func main() {
	fmt.Println("Test case 1")
	obj := lruCache.NewLRUCache(2)
	obj.Put(1, "test 1")
	obj.Put(2, "test 2")
	fmt.Println(obj.Get(1))
	obj.Put(3, "test 3")
	fmt.Println(obj.Get(2))
	obj.Put(4, "test 4")
	fmt.Println(obj.Get(1))
	fmt.Println(obj.Get(3))
	fmt.Println(obj.Get(4))
}
