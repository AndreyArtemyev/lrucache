package lruCache

import "container/list"

type hashTable struct {
	key   int
	value string
}

type LRUCache struct {
	capacity int
	list     *list.List
	elements map[int]*list.Element
}

func NewLRUCache(capacity int) LRUCache {
	return LRUCache{capacity: capacity, list: list.New(), elements: make(map[int]*list.Element, capacity)}
}

func (cache *LRUCache) Put(key int, value string) {
	if node, ok := cache.elements[key]; ok {
		cache.list.MoveToFront(node)
		node.Value.(*list.Element).Value = hashTable{key: key, value: value}
	} else {
		if cache.list.Len() == cache.capacity {
			idx := cache.list.Back().Value.(*list.Element).Value.(hashTable).key
			delete(cache.elements, idx)
			cache.list.Remove(cache.list.Back())
		}
	}

	node := &list.Element{
		Value: hashTable{
			key:   key,
			value: value,
		},
	}

	pointer := cache.list.PushFront(node)
	cache.elements[key] = pointer
}

func (cache *LRUCache) Get(key int) (string, bool) {
	if node, ok := cache.elements[key]; ok {
		value := node.Value.(*list.Element).Value.(hashTable).value
		cache.list.MoveToFront(node)
		return value, true
	}
	return "", false
}

func (cache *LRUCache) Remove(key int) {
	if node, ok := cache.elements[key]; ok {
		delete(cache.elements, key)
		cache.list.Remove(node)
	}
}
